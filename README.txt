Out Of Context is built on top of Context module, in order to achieve an easy to use, in-line blocks editor.
It creates a nice palette where you can select the context you want to edit, and a list of blocks which can be dragged & dropped to the different regions.
A configuration screen lets you select which contexts will activate the palette, and also to select the blocks which will appear in it.

In addition, the module has a Lobby Pages Creator, which lets you select different layouts for each content type set on the site, and adds an �On Current Page Only� context for each lobby page.

Dependencies:
Context